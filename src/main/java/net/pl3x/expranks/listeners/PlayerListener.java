package net.pl3x.expranks.listeners;

import net.pl3x.expranks.ExpRanks;
import net.pl3x.expranks.tasks.RankCheck;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.enchantment.EnchantItemEvent;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.player.PlayerExpChangeEvent;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerQuitEvent;
import org.bukkit.inventory.AnvilInventory;

public class PlayerListener implements Listener {
	private ExpRanks plugin;
	
	public PlayerListener(ExpRanks plugin) {
		this.plugin = plugin;
	}
	
	@EventHandler(priority = EventPriority.MONITOR)
	public void playerJoin(PlayerJoinEvent event) {
		Player player = event.getPlayer();
		if (player.isOp())
			return;
		plugin.debug("Player Joined! Setting Rank.");
		plugin.getRankManager().addPlayerRank(player);
		if (!plugin.getConfig().getBoolean("can-demote-ranks", true) || player.hasPermission("expranks.nodemote"))
			return;
		if (plugin.getConfig().getBoolean("set-rank-on-join", false))
			plugin.getRankManager().setPlayerRank(player);
	}
	
	@EventHandler(priority = EventPriority.MONITOR)
	public void playerQuit(PlayerQuitEvent event) {
		if (event.getPlayer().isOp())
			return;
		plugin.debug("Player Quit! Removing Rank.");
		plugin.getRankManager().removePlayerRank(event.getPlayer().getName());
	}
	
	@EventHandler(priority = EventPriority.MONITOR)
	public void playerExpChange(PlayerExpChangeEvent event) {
		plugin.debug("Player Exp Changed, Checking Rank.");
		Bukkit.getScheduler().runTaskLater(plugin, new RankCheck(plugin, event.getPlayer()), 1);
	}
	
	@EventHandler(priority = EventPriority.MONITOR, ignoreCancelled = true)
	public void enchantItem(EnchantItemEvent event) {
		plugin.debug("Player Enchanted on Table, Checking Rank.");
		Bukkit.getScheduler().runTaskLater(plugin, new RankCheck(plugin, event.getEnchanter()), 1);
	}
	
	@EventHandler(priority = EventPriority.MONITOR, ignoreCancelled = true)
	public void anvilEnchant(InventoryClickEvent event) {
		if(!(event.getInventory() instanceof AnvilInventory))
			return;
		int rawSlot = event.getRawSlot();
		if(rawSlot != event.getView().convertSlot(rawSlot))
			return;
		if(rawSlot != 2)
			return;
		plugin.debug("Player Enchanted on Anvil, Checking Rank.");
		Bukkit.getScheduler().runTaskLater(plugin, new RankCheck(plugin, (Player) event.getWhoClicked()), 1);
	}
}
