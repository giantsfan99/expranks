package net.pl3x.expranks.rank;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import net.pl3x.expranks.ExpRanks;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;

public class RankManager {
	private ExpRanks plugin;
	private HashMap<Integer,String> systemRanks = new HashMap<Integer,String>();
	private HashMap<String,Rank> playerRanks = new HashMap<String,Rank>();
	
	public RankManager(ExpRanks plugin) {
		this.plugin = plugin;
		loadRanks();
	}
	
	public String getSystemRank(Integer level) {
		int i = 0;
		while (!systemRanks.containsKey(level - i)) {
			plugin.debug("&rNo rank for level &e" + (level - i) + "&r. Trying again...");
			if (level - i <= 0) {
				plugin.debug("No rank found! Giving up...");
				return null;
			}
			i++;
		}
		return systemRanks.get(level - i);
	}
	
	public HashMap<String,Rank> getPlayerRanks() {
		return playerRanks;
	}
	
	public RankManager addPlayerRank(Player player) {
		String rankStr = getSystemRank(player.getLevel());
		if (rankStr == null)
			rankStr = plugin.getConfig().getString("default-rank", "default");
		Rank rank = new Rank(player.getLevel(), rankStr);
		playerRanks.put(player.getName(), rank);
		return this;
	}
	
	public void removePlayerRank(String name) {
		if (playerRanks.containsKey(name))
			playerRanks.remove(name);
	}
	
	public void setPlayerRank(Player player) {
		String rankCommand = null;
		try {
			rankCommand = plugin.getConfig().getString("rank-command");
		} catch (Exception e) {
			plugin.log("&4ERROR: rank-command is not set in config.yml!");
			return;
		}
		if (rankCommand == null) {
			plugin.log("&4ERROR: rank-command is not set in config.yml!");
			return;
		}
		rankCommand = rankCommand.replaceAll("(?i)\\{name\\}", player.getName());
		String rankStr = getSystemRank(player.getLevel());
		if (rankStr == null)
			rankStr = plugin.getConfig().getString("default-rank", "default");
		rankCommand = rankCommand.replaceAll("(?i)\\{rank\\}", rankStr);
		plugin.debug("Performing set rank command: &e/" + rankCommand);
		Bukkit.getServer().dispatchCommand(Bukkit.getConsoleSender(), rankCommand);
		if (plugin.getConfig().getBoolean("notify-player", true))
			player.sendMessage(plugin.colorize(plugin.getConfig().getString("notify-message", "&dYour rank has been updated.")));
	}
	
	public void loadRanks() {
		List<Map<?,?>> ranks = plugin.getConfig().getMapList("ranks");
		if (ranks == null || ranks.isEmpty())
			return;
		for (Map<?,?> rank : ranks) {
			for (Object obj : rank.keySet()) {
				Integer level = null;
				try {
					level = (Integer) obj;
				} catch(Exception e) {
					plugin.debug("Invalid rank level: &4" + e.getMessage());
					continue;
				}
				String rankName = null;
				try {
					rankName = (String) rank.get(level);
				} catch(Exception e) {
					plugin.debug("Invalid rank name: &4" + e.getMessage());
					continue;
				}
				systemRanks.put(level, rankName);
				plugin.debug("&rAdded rank: &6" + rankName + " &rLevel: &6" + level);
			}
		}
	}
	
	public void clear() {
		systemRanks.clear();
		playerRanks.clear();
	}
}
