package net.pl3x.expranks.rank;


public class Rank {
	private Integer level;
	private String rank;
	
	public Rank(Integer level, String rank) {
		this.level = level;
		this.rank = rank;
	}
	
	public Integer getLevel() {
		return level;
	}
	
	public void setLevel(Integer level) {
		this.level = level;
	}
	
	public String getRank() {
		return rank;
	}
	
	public void setRank(String rank) {
		this.rank = rank;
	}
}
