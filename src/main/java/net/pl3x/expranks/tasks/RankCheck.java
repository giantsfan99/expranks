package net.pl3x.expranks.tasks;

import net.pl3x.expranks.ExpRanks;
import net.pl3x.expranks.rank.Rank;

import org.bukkit.entity.Player;

public class RankCheck implements Runnable {
	private ExpRanks plugin;
	private Player player;
	
	public RankCheck(ExpRanks plugin, Player player) {
		this.plugin = plugin;
		this.player = player;
	}

	@Override
	public void run() {
		if (player.isOp())
			return;
		Rank pRank = plugin.getRankManager().getPlayerRanks().get(player.getName());
		if (pRank.getLevel() == player.getLevel())
			return;
		if (player.getLevel() < pRank.getLevel())
			if (!plugin.getConfig().getBoolean("can-demote-ranks", true) || player.hasPermission("expranks.nodemote"))
				return;
		pRank.setLevel(player.getLevel());
		plugin.debug("Player Level Changed! Checking Rank.");
		String rankName = plugin.getRankManager().getSystemRank(pRank.getLevel());
		if (rankName == null)
			rankName = plugin.getConfig().getString("default-rank", "default");
		if (pRank.getRank().equals(rankName)) {
			return;
		}
		pRank.setRank(rankName);
		plugin.debug("Player Rank Changed! Setting New Rank.");
		plugin.getRankManager().setPlayerRank(player);
	}

}
